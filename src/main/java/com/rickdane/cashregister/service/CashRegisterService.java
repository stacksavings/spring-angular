package com.rickdane.cashregister.service;

import com.rickdane.cashregister.model.Transaction;

import java.util.Map;

public interface CashRegisterService {

    public Transaction performTransaction (Transaction transaction);


}
