package com.rickdane.cashregister.service;

import com.rickdane.cashregister.config.AppConfig;
import com.rickdane.cashregister.model.Transaction;
import org.junit.*;

import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Map;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class, loader = AnnotationConfigContextLoader.class)
public class CashRegisterServiceTest {

    @Test
    public void test1() {

        CashRegisterService service = new CashRegisterServiceImpl();

        Transaction transaction = new Transaction();

        transaction.setCntTwenties(20);

        transaction.setPrice("160");
        Transaction changeContainer = service.performTransaction(transaction);

        Assert.assertEquals(changeContainer.getCntTwenties(), 10);
        Assert.assertEquals(changeContainer.getCntTens(), 4);

        changeContainer = service.performTransaction(transaction);
        Assert.assertEquals(changeContainer.getError(), "There is not enough change in the register, transaction is canceled.");

    }

    @Test
    public void test2() {

        CashRegisterService service = new CashRegisterServiceImpl();

        Transaction transaction = new Transaction();

        transaction.setCntTwenties(1);

        transaction.setPrice("40");
        Transaction changeContainer = service.performTransaction(transaction);

        Assert.assertEquals(changeContainer.getError(), "The amount of money paid is not enough for the cost of the item, transaction is cancelled.");

    }

    @Test
    public void test3() {

        CashRegisterService service = new CashRegisterServiceImpl();

        Transaction transaction = new Transaction();

        transaction.setCntFives(3);

        transaction.setPrice("12.01");
        Transaction changeContainer = service.performTransaction(transaction);

        Assert.assertEquals(changeContainer.getCntDollars(), 2);
        Assert.assertEquals(changeContainer.getCntQuarters(), 3);
        Assert.assertEquals(changeContainer.getCntDimes(), 2);
        Assert.assertEquals(changeContainer.getCntPennies(), 4);
        Assert.assertEquals(changeContainer.getTotal(), "2.99");

    }

}