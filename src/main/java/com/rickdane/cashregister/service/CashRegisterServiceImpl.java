package com.rickdane.cashregister.service;

import com.rickdane.cashregister.model.Transaction;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@Service("carService")
public class CashRegisterServiceImpl implements CashRegisterService {


    static Map<BigDecimal, Integer> register = new HashMap<BigDecimal, Integer>();
    private static BigDecimal penny = new BigDecimal(".01");
    private static BigDecimal nickel = new BigDecimal(".05");
    private static BigDecimal dime = new BigDecimal(".10");
    private static BigDecimal quarter = new BigDecimal(".25");
    private static BigDecimal dollar = new BigDecimal("1.00");
    private static BigDecimal five = new BigDecimal("5.00");
    private static BigDecimal ten = new BigDecimal("10.00");
    private static BigDecimal twenty = new BigDecimal("20.00");

    private static BigDecimal zero = new BigDecimal("0.00");
    private static BigDecimal negativeOne = new BigDecimal("-1.00");

    public CashRegisterServiceImpl() {
        if (register.isEmpty()) {
            //stock cash register with initial currency
            register.put(penny, 10);
            register.put(nickel, 10);
            register.put(dime, 10);
            register.put(quarter, 10);
            register.put(dollar, 10);
            register.put(five, 10);
            register.put(ten, 10);
            register.put(twenty, 10);
        }

    }


    public Transaction performTransaction (Transaction transaction) {

        BigDecimal total = new BigDecimal("0.00");

        BigDecimal amount = new BigDecimal(transaction.getPrice());

        Transaction changeContainer = new Transaction();

        total = total.add(penny.multiply(new BigDecimal(transaction.getCntPennies())));
        total = total.add(nickel.multiply(new BigDecimal(transaction.getCntNickels())));
        total = total.add(dime.multiply(new BigDecimal(transaction.getCntDimes())));
        total = total.add(quarter.multiply(new BigDecimal(transaction.getCntQuarters())));
        total = total.add(dollar.multiply(new BigDecimal(transaction.getCntDollars())));
        total = total.add(five.multiply(new BigDecimal(transaction.getCntFives())));
        total = total.add(ten.multiply(new BigDecimal(transaction.getCntTens())));
        total = total.add(twenty.multiply(new BigDecimal(transaction.getCntTwenties())));

        if (total.compareTo(amount) == -1 ) {
            changeContainer.setError("The amount of money paid is not enough for the cost of the item, transaction is cancelled.");
        } else if (total.compareTo(amount) == 1) {

            BigDecimal amtRemaining = total.subtract(amount);

            BigDecimal totalChange = zero;

            while (amtRemaining.compareTo(zero) == 1) {

                BigDecimal withdrawnAmt = withdraw(amtRemaining, changeContainer);

                totalChange = totalChange.add(withdrawnAmt);

                if (withdrawnAmt.compareTo(zero) == -1 ) {
                    changeContainer.setError("There is not enough change in the register, transaction is canceled.");
                    break;
                }

                amtRemaining = amtRemaining.subtract(withdrawnAmt);

            }

            changeContainer.setTotal(totalChange.toString());
        }

        if (!StringUtils.isEmpty(changeContainer.getError())) {
            clearTransactionValues(changeContainer);
        }
        return changeContainer;

    }

    private static void clearTransactionValues(Transaction transaction) {
        transaction.setCntPennies(0);
        transaction.setCntNickels(0);
        transaction.setCntDimes(0);
        transaction.setCntQuarters(0);
        transaction.setCntDollars(0);
        transaction.setCntFives(0);
        transaction.setCntTens(0);
        transaction.setCntTwenties(0);
        transaction.setTotal("0");
    }

    public BigDecimal withdraw(BigDecimal amt, Transaction changeContainer) {

        if (amt.compareTo(zero) < 1) {
            return zero;
        }

        if (amt.compareTo(twenty) > -1  && register.get(twenty) > 0) {
            changeContainer.setCntTwenties(changeContainer.getCntTwenties()+1);
            register.put(twenty, register.get(twenty) - 1);
            return twenty;
        }

        if (amt.compareTo(ten) > -1  && register.get(ten) > 0) {
            changeContainer.setCntTens(changeContainer.getCntTens()+1);
            register.put(ten, register.get(ten)  - 1);
            return ten;
        }

        if (amt.compareTo(five) > -1 && register.get(five) > 0) {
            changeContainer.setCntFives(changeContainer.getCntFives()+1);
            register.put(five, register.get(five) - 1);
            return five;
        }

        if (amt.compareTo(dollar) > -1  && register.get(dollar) > 0) {
            changeContainer.setCntDollars(changeContainer.getCntDollars()+1);
            register.put(dollar, register.get(dollar) - 1);
            return dollar;
        }

        if (amt.compareTo(quarter) > -1  && register.get(quarter) > 0) {
            changeContainer.setCntQuarters(changeContainer.getCntQuarters()+1);
            register.put(quarter, register.get(quarter)  - 1);
            return quarter;
        }

        if (amt.compareTo(dime) > -1   && register.get(dime) > 0) {
            changeContainer.setCntDimes(changeContainer.getCntDimes()+1);
            register.put(dime, register.get(dime)  - 1);
            return dime;
        }

        if (amt.compareTo(nickel) > -1  && register.get(nickel) > 0) {
            changeContainer.setCntNickels(changeContainer.getCntNickels()+1);
            register.put(nickel, register.get(nickel)  - 1);
            return nickel;
        }

        if (amt.compareTo(penny) > -1  && register.get(penny) > 0) {
            changeContainer.setCntPennies(changeContainer.getCntPennies()+1);
            register.put(penny, register.get(penny)  - 1);
            return penny;
        }

        return negativeOne;

    }

    public Map<BigDecimal, Integer> getRegister() {
        return register;
    }


}
