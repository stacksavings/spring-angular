package com.rickdane.cashregister.model;


public class Transaction {


    //private Map<Double, Integer> transaction = new HashMap<Double, Integer>();

    int cntPennies;
    int cntNickels;
    int cntDimes;
    int cntQuarters;
    int cntDollars;
    int cntFives;
    int cntTens;
    int cntTwenties;

    String price;
    String total;
    String error;

    public Transaction() {

    }

    public int getCntPennies() {
        return cntPennies;
    }

    public void setCntPennies(int cntPennies) {
        this.cntPennies = cntPennies;
    }

    public int getCntNickels() {
        return cntNickels;
    }

    public void setCntNickels(int cntNickels) {
        this.cntNickels = cntNickels;
    }

    public int getCntDimes() {
        return cntDimes;
    }

    public void setCntDimes(int cntDimes) {
        this.cntDimes = cntDimes;
    }

    public int getCntQuarters() {
        return cntQuarters;
    }

    public void setCntQuarters(int cntQuarters) {
        this.cntQuarters = cntQuarters;
    }

    public int getCntDollars() {
        return cntDollars;
    }

    public void setCntDollars(int cntDollars) {
        this.cntDollars = cntDollars;
    }

    public int getCntFives() {
        return cntFives;
    }

    public void setCntFives(int cntFives) {
        this.cntFives = cntFives;
    }

    public int getCntTens() {
        return cntTens;
    }

    public void setCntTens(int cntTens) {
        this.cntTens = cntTens;
    }

    public int getCntTwenties() {
        return cntTwenties;
    }

    public void setCntTwenties(int cntTwenties) {
        this.cntTwenties = cntTwenties;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
