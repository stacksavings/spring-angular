package com.rickdane.cashregister.controller;

import com.rickdane.cashregister.model.Transaction;
import com.rickdane.cashregister.service.CashRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/cashregister")
public class CashRegisterController {

    @Autowired
    private CashRegisterService cashRegisterService;


    @RequestMapping(value = "/performTransaction", method = RequestMethod.POST)
    public @ResponseBody Transaction performTransaction(@RequestBody Transaction transaction) {

        System.out.println("---" + transaction.getPrice() + "---");

        return cashRegisterService.performTransaction(transaction);
    }


}
