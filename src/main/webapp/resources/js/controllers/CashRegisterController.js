'use strict';

/**
 * CashRegisterController
 * @constructor
 */
var CashRegisterController = function($scope, $http) {

    $scope.submitForm = function() {

      var reqObj = {}
      reqObj.cntPennies = $scope.form.cntPennies
      reqObj.cntNickels = $scope.form.cntNickels
      reqObj.cntDimes = $scope.form.cntDimes
      reqObj.cntQuarters = $scope.form.cntQuarters
      reqObj.cntDollars = $scope.form.cntDollars
      reqObj.cntFives = $scope.form.cntFives
      reqObj.cntTens = $scope.form.cntTens
      reqObj.cntTwenties = $scope.form.cntTwenties
      reqObj.price = $scope.form.price

        var returnData =  $http.post('cashregister/performTransaction', reqObj, {headers: {'Content-Type': 'application/json'}}).success(function(response){
		$scope.message = JSON.stringify(response);
		$scope.showResult = true

		$scope.error = response.error

      $scope.cntPennies = response.cntPennies
      $scope.cntNickels = response.cntNickels
      $scope.cntDimes = response.cntDimes
      $scope.cntQuarters = response.cntQuarters
      $scope.cntDollars = response.cntDollars
      $scope.cntFives = response.cntFives
      $scope.cntTens = response.cntTens
      $scope.cntTwenties = response.cntTwenties

      $scope.total = response.total

	    console.log($scope.message);

	});


    };


};
